import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppstructureComponent } from './appstructure.component';

describe('AppstructureComponent', () => {
  let component: AppstructureComponent;
  let fixture: ComponentFixture<AppstructureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppstructureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppstructureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
