import {Component, OnInit} from '@angular/core';
import {data} from '../../infos';
import {ColorPickerBackgroundService} from '../../service/color-picker-background.service';

@Component({
  selector: 'app-appstructure',
  templateUrl: './appstructure.component.html',
  styleUrls: ['./appstructure.component.scss']
})
export class AppstructureComponent implements OnInit {
  userChoice = data;
  public mozaic: boolean;

  constructor(private colorPickerService: ColorPickerBackgroundService) {
    this.colorPickerService.getDisplay().subscribe(value => {
      console.log(value);
      this.mozaic = value;
    });
  }

  ngOnInit() {
  }

}
