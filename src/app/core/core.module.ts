import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { SharedModule } from '../shared/shared.module';
import { AppstructureComponent } from './appstructure/appstructure.component';
import { FeatureModule } from '../feature/feature.module';



@NgModule({
  declarations: [ToolbarComponent, AppstructureComponent],
  imports: [
    CommonModule,
    SharedModule,
    FeatureModule,
  ],
  exports: [
    ToolbarComponent,
    AppstructureComponent
  ]
})
export class CoreModule { }
