import { Component, OnInit } from '@angular/core';
import { data } from '../../infos';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  userChoice = data;

  constructor() { }

  ngOnInit() {
  }

}
