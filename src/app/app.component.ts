import { Component } from '@angular/core';
import {ColorPickerBackgroundService} from './service/color-picker-background.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'colab-frontend';

  backgroundColor: string;
  constructor(private colorPickerBackgroundService: ColorPickerBackgroundService) {
    this.colorPickerBackgroundService.getBackgroundColor().subscribe(value => {
      this.backgroundColor = value;
    });
  }
}
