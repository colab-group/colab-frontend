export const data = {
    title: 'Videos Sport',
    company: 'Scorebat',
    url: '',
    displayEnable: true,
    displayDefault: 'LIST',
    displays: ['LIST', 'MOZAIC'],

    searchbar: true,
    // nbVideos: 20,
    color: '#eeeeee',
    colorPicker: true,
    apis: ['DAILYMOTION', 'DEEZER'],
    sortresult: true
};
