import { TestBed } from '@angular/core/testing';

import { ApiStandardService } from './api-standard.service';

describe('ApiStandardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApiStandardService = TestBed.get(ApiStandardService);
    expect(service).toBeTruthy();
  });
});
