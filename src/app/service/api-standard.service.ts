import {Injectable} from '@angular/core';
import {environment} from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {StandardApiModel} from '../models/StandardApiModel';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiStandardService {

  private backend: string = environment.backendUrl;

  private dataSubject: Subject<StandardApiModel> = new Subject<StandardApiModel>();
  private dataBaseSubject: StandardApiModel;
  private filterName: string;
  private SortData: any[];

  constructor(private http: HttpClient) {
    this.dataBaseSubject = null;
    this.filterName = null;
    this.SortData = null;
    this.updateData();
  }

  getApiData(): Observable<StandardApiModel> {
    return this.dataSubject;
  }

  updateData(): void {
    this.http.get<StandardApiModel>(this.backend).subscribe(value => {
      this.dataBaseSubject = value;
      this.applyFilters();
    });
  }

  public handleFilterName(name: string) {
    this.filterName = name;
    this.applyFilters();
  }
  public applyFilters() {
    let stdModel: StandardApiModel = this.dataBaseSubject;
    stdModel = this.filters(stdModel);
    stdModel = this.sort(stdModel);
    this.dataSubject.next(stdModel);
  }

  private filters(data: StandardApiModel): StandardApiModel {
    data = this.filterNameExec(data);
    return data;
  }

  /**
   * Filtre par nom
   *  - Dailymotion : titre de la video
   *  - Deezer: titre de la chanson
   *  - Scorebat: titre
   */
  private filterNameExec(data: StandardApiModel): StandardApiModel {
    console.log(this.filterName);
    if (data !== null && this.filterName !== null) {
      const out: StandardApiModel = {
        dailymotion: null,
        deezer: null,
        scorebat: null
      };
      if (data.dailymotion !== null && data.dailymotion !== undefined) {
        out.dailymotion = data.dailymotion.filter(value => value.title.includes(this.filterName));
      }
      if (data.deezer !== null && data.deezer !== undefined) {
        out.deezer = {
          data: data.deezer.data.filter(value => value.title.includes(this.filterName)),
            next: data.deezer.next,
            total: data.deezer.total
        };
      }
      if (data.scorebat !== null && data.scorebat !== undefined) {
        out.scorebat = data.scorebat.filter(value => value.title.includes(this.filterName));
      }
      return out;
    }
    return data;
  }

  public handleSort(field: string, isAscend: boolean) {
    this.SortData = [field, isAscend];
    this.applyFilters();
  }

  private sort(data: StandardApiModel): StandardApiModel {
    data = this.sortExec(data);
    return data;
  }

  private sortExec(data: StandardApiModel): StandardApiModel {
    let tempData;
    if (data !== null && this.SortData !== null) {
      const out: StandardApiModel = {
        dailymotion: null,
        deezer: null,
        scorebat: null
      };
      if (data.dailymotion !== null) {
        tempData = null;
        if (this.SortData[0] === 'Author') {
          if (this.SortData[1]) {
            tempData = data.dailymotion.sort((a, b) =>
              a.owner < b.owner ? -1 : 1
            );
          } else {
            tempData = data.dailymotion.sort((a, b) =>
              a.owner < b.owner ? 1 : -1
            );
          }
        }
        if (this.SortData[0] === 'Title') {
          if (this.SortData[1]) {
            tempData = data.dailymotion.sort((a, b) =>
              a.title < b.title ? -1 : 1
            );
          } else {
            tempData = data.dailymotion.sort((a, b) =>
              a.title < b.title ? 1 : -1
            );
          }
        }
        out.dailymotion = tempData;
      }
      if (data.deezer !== null) {
        tempData = null;
        if (this.SortData[0] === 'Author') {
          if (this.SortData[1]) {
            tempData = data.deezer.data.sort((a, b) =>
              a.artist < b.artist ? -1 : 1
            );
          } else {
            tempData = data.deezer.data.sort((a, b) =>
              a.artist < b.artist ? 1 : -1
            );
          }
        }
        if (this.SortData[0] === 'Title') {
          if (this.SortData[1]) {
            tempData = data.deezer.data.sort((a, b) =>
              a.title < b.title ? -1 : 1
            );
          } else {
            tempData = data.deezer.data.sort((a, b) =>
              a.title < b.title ? 1 : -1
            );
          }
        }
        out.deezer = {
          data: tempData,
          next: data.deezer.next,
          total: data.deezer.total
        };
      }
      return out;
    }
    return data;
  }
}
