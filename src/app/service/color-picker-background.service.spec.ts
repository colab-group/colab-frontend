import { TestBed } from '@angular/core/testing';

import { ColorPickerBackgroundService } from './color-picker-background.service';

describe('ColorPickerBackgroundService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ColorPickerBackgroundService = TestBed.get(ColorPickerBackgroundService);
    expect(service).toBeTruthy();
  });
});
