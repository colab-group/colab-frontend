import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {data} from '../infos';

@Injectable({
  providedIn: 'root'
})
export class ColorPickerBackgroundService {

  public colorSubject: BehaviorSubject<string> = new BehaviorSubject<string>(data.color);
  public displaySubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(data.displayDefault === 'MOZAIC');
  constructor() {}

  setBackgroundColor(color: string) {
    this.colorSubject.next(color);
  }
  getBackgroundColor(): Observable<string> {
    return this.colorSubject;
  }
  setDisplay(value: boolean) {
    this.displaySubject.next(value);
  }
  getDisplay(): Observable<boolean> {
    return this.displaySubject;
  }
}
