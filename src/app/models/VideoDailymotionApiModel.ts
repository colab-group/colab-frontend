export interface VideoDailymotionApiModel {
  channel: string;
  id: string;
  owner: string;
  title: string;
}
