import { VideoDailymotionApiModel } from './VideoDailymotionApiModel';
import { ScoreBatApiModel } from './ScoreBatApiModel';
import { DeezerApiModel } from './DeezerApiModel';

export interface StandardApiModel {
  scorebat: ScoreBatApiModel[];
  dailymotion: VideoDailymotionApiModel[];
  deezer: DeezerApiModel;
}
