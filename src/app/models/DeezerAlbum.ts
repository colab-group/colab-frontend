export interface DeezerAlbum {
  cover: string;
  cover_big: string;
  cover_medium: string;
  cover_small: string;
  cover_xl: string;
  id: number;
  title: string;
  tracklist: string;
  type: string;
}
