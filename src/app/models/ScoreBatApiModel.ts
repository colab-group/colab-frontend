import { VideoScoreBatApiModel } from './VideoScoreBatApiModel';

export interface ScoreBatApiModel {
  embed: string;
  thumbnail: string;
  title: string;
  url: string;
  videos: VideoScoreBatApiModel[];
}
