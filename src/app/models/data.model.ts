export interface Video {
  thumbnail: string;
  tag: string;
  name: string;
  title: string;
  date: string;
  videos: Videos;
}

export interface Videos {
  title: string;
  url: string;
}

export interface StandardApi {
  scorebat: Video[];
  dailymotion: DailyMotion[];
}

export interface DailyMotion {
    id: string;
    title: string;
    channel: string;
    owner: string;
}
