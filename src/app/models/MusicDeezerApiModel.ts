import { DeezerAlbum } from './DeezerAlbum';
import { DeezerArtist } from './DeezerArtist';

export interface MusicDeezerApiModel {
  album: DeezerAlbum;
  artist: DeezerArtist;
  duration: number;
  explicit_content_cover: number;
  explicit_content_lyrics: number;
  explicit_lyrics: boolean;
  id: number;
  link: string;
  preview: string;
  rank: number;
  readable: boolean;
  title: string;
  title_short: string;
  title_version: string;
  type: string;
}
