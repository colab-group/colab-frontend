import {MusicDeezerApiModel} from './MusicDeezerApiModel';

export interface DeezerApiModel {
  data: MusicDeezerApiModel[];
  next: string;
  total: number;
}
