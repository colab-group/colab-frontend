import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ColorPickerBackgroundService} from '../../../../service/color-picker-background.service';
import {data} from '../../../../infos';

interface DialogData {
  name: string;
  animal: string;
}

@Component({
  selector: 'app-dialog-settings',
  templateUrl: './dialog-settings.component.html',
  styleUrls: ['./dialog-settings.component.scss']
})
export class DialogSettingsComponent {

  userInfo = data;
  mozaic: boolean;

  constructor(public dialogRef: MatDialogRef<DialogSettingsComponent>,
              @Inject(MAT_DIALOG_DATA) public data1: DialogData,
              private colorPickerBackgroundService: ColorPickerBackgroundService) {
    colorPickerBackgroundService.getDisplay().subscribe(value => {
      console.log(value);
      this.mozaic = value;
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  change() {
      this.colorPickerBackgroundService.setDisplay(!this.mozaic);
  }
}
