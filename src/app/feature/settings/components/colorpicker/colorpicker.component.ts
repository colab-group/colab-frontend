import {Component} from '@angular/core';

import {ColorPickerService} from 'ngx-color-picker';
import {ColorPickerBackgroundService} from '../../../../service/color-picker-background.service';
import {data} from "../../../../infos";

@Component({
  selector: 'app-colorpicker',
  templateUrl: './colorpicker.component.html',
  styleUrls: ['./colorpicker.component.scss']
})
export class ColorpickerComponent {

  public colorList = [
    { key: 'flame', value: '#e45a33', friendlyName: 'Flame' },
    {key: 'orange', value: '#fa761e', friendlyName: 'Orange' },
    {key: 'infrared',     value: '#ef486e', friendlyName: 'Infrared' },
    {key: 'male',       value: '#4488ff', friendlyName: 'Male Color' },
    {key: 'female',     value: '#ff44aa', friendlyName: 'Female Color' },
    {key: 'paleyellow',    value: '#ffd165', friendlyName: 'Pale Yellow' },
    {key: 'gargoylegas',  value: '#fde84e', friendlyName: 'Gargoyle Gas' },
    {key: 'androidgreen',   value: '#9ac53e', friendlyName: 'Android Green' },
    {key: 'carribeangreen',    value: '#05d59e', friendlyName: 'Carribean Green' },
    {key: 'bluejeans',    value: '#5bbfea', friendlyName: 'Blue Jeans' },
    {key: 'cyancornflower',    value: '#1089b1', friendlyName: 'Cyan Cornflower' },
    {key: 'warmblack',    value: '#06394a', friendlyName: 'Warm Black' },
  ];


  public presetValues: string[] = [];

  public selectedColor: string;

  constructor(private cpService: ColorPickerService,
              private colorPickerBackgroundService: ColorPickerBackgroundService) {

    if (data.color !== undefined) {
      this.selectedColor = data.color;
    } else {
      this.selectedColor = '#eeeeee';
    }

    this.presetValues = this.getColorValues();
  }

  getColorValues() {
    return this.colorList.map(c => c.value);
  }

  public handleChangeColor(color: string) {
    this.colorPickerBackgroundService.setBackgroundColor(color);
  }
}

