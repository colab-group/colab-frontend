import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ColorPickerModule} from 'ngx-color-picker';
import {ColorpickerComponent} from './components/colorpicker/colorpicker.component';
import {SettingsComponent} from './containers/settings/settings.component';
import {SharedModule} from '../../shared/shared.module';
import {DialogSettingsComponent} from './components/dialog-settings/dialog-settings.component';

@NgModule({
  declarations: [ColorpickerComponent, SettingsComponent, DialogSettingsComponent],
  imports: [
    CommonModule,
    SharedModule,
    ColorPickerModule

  ],
  entryComponents: [
    DialogSettingsComponent
  ],
  exports: [SettingsComponent]
})
export class SettingsModule { }
