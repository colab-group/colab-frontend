import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {SearchbarModule} from './searchbar/searchbar.module';
import {ItemslistModule} from './itemslist/itemslist.module';
import {SettingsModule} from './settings/settings.module';
import {SortModule} from './sort/sort.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SharedModule,
    SearchbarModule,
    ItemslistModule,
    SettingsModule,
    SortModule
  ],
    exports: [
      SearchbarModule,
      ItemslistModule,
      SettingsModule,
      SortModule
    ],
})
export class FeatureModule { }
