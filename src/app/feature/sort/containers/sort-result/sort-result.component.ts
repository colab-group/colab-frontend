import {Component, OnInit} from '@angular/core';
import {ApiStandardService} from '../../../../service/api-standard.service';

@Component({
  selector: 'app-sorte-result',
  templateUrl: './sort-result.component.html',
  styleUrls: ['./sort-result.component.scss']
})
export class SortResultComponent implements OnInit {
  public display: boolean;
  public fields: string[];
  public fieldSelected: string;
  public isAscend: boolean;

  constructor(private apiStandardService: ApiStandardService) {
    this.display = false;
    this.fields = ['Title', 'Author'];
    this.fieldSelected = '';
    this.isAscend = true;
  }

  ngOnInit() {
  }

  displaySorte() {
    this.display = !(this.display);
  }

  doSort() {
    this.apiStandardService.handleSort(this.fieldSelected, this.isAscend);
  }
}
