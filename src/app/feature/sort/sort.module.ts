import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SortResultComponent} from './containers/sort-result/sort-result.component';
import {SharedModule} from '../../shared/shared.module';


@NgModule({
  declarations: [SortResultComponent],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [SortResultComponent]
})
export class SortModule { }
