import { Component, OnInit } from '@angular/core';
import {ApiStandardService} from '../../../service/api-standard.service';

@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.scss']
})
export class SearchbarComponent implements OnInit {

  constructor(private apiStandardService: ApiStandardService) { }

  ngOnInit() {

  }

  handleChangeText(text: string) {
    this.apiStandardService.handleFilterName(text);
  }

}
