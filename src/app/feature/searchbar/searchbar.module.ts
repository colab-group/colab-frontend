import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { SearchbarComponent } from './searchbar/searchbar.component';



@NgModule({
  declarations: [SearchbarComponent],
  imports: [
    CommonModule,
    SharedModule,
  ],
  exports : [
    SearchbarComponent,
  ]
})
export class SearchbarModule { }
