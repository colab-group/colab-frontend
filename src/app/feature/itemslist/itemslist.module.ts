import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from 'src/app/shared/shared.module';
import {ItemslistComponent} from './containers/itemslist/itemslist.component';
import {ScorbatitemComponent} from './components/scorbatitem/scorbatitem.component';
import {DeezeritemComponent} from './components/deezeritem/deezeritem.component';
import {DailymotionitemComponent} from './components/dailymotionitem/dailymotionitem.component';


@NgModule({
  declarations: [ItemslistComponent, ScorbatitemComponent, DeezeritemComponent, DailymotionitemComponent],
  imports: [
    CommonModule,
    SharedModule,
  ],
  exports : [
    ItemslistComponent
  ]
})
export class ItemslistModule { }
