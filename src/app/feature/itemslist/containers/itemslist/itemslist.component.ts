import {Component, Input, OnInit} from '@angular/core';
import {StandardApiModel} from '../../../../models/StandardApiModel';
import {ApiStandardService} from '../../../../service/api-standard.service';
import {data} from '../../../../infos';

@Component({
  selector: 'app-itemslist',
  templateUrl: './itemslist.component.html',
  styleUrls: ['./itemslist.component.scss']
})
export class ItemslistComponent implements OnInit {

  @Input() display: string;
  public applicationProperty = data;
  datas: StandardApiModel;

  constructor(private apiStandardService: ApiStandardService) {
    this.datas = {
      deezer: null,
      dailymotion: null,
      scorebat: null
    };
  }

  ngOnInit() {
    this.apiStandardService.getApiData().subscribe((standardApiData: StandardApiModel) => {
      console.log(standardApiData);
      this.datas = {
        deezer: standardApiData.deezer,
        dailymotion: standardApiData.dailymotion,
        scorebat: standardApiData.scorebat,
      };
    });
  }

}
