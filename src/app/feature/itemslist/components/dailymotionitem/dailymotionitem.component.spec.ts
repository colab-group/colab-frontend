import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailymotionitemComponent } from './dailymotionitem.component';

describe('DailymotionitemComponent', () => {
  let component: DailymotionitemComponent;
  let fixture: ComponentFixture<DailymotionitemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailymotionitemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailymotionitemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
