import {Component, Input, OnInit} from '@angular/core';
import {VideoDailymotionApiModel} from 'src/app/models/VideoDailymotionApiModel';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';

@Component({
  selector: 'app-dailymotionitem',
  templateUrl: './dailymotionitem.component.html',
  styleUrls: ['./dailymotionitem.component.scss']
})
export class DailymotionitemComponent implements OnInit {

  @Input() dailymotionvideo: VideoDailymotionApiModel;

  controllerSrc: SafeResourceUrl;

  width: number;
  height: number;

  constructor(private sanitizer: DomSanitizer) {
    // this.setWidth(450);
    this.setWidth(300);
  }

  ngOnInit() {
    this.controllerSrc = this.sanitizer.bypassSecurityTrustResourceUrl(this.dailymotionvideo.id);
  }

  setWidth(width: number) {
    this.width = width;
    this.height = width * 0.56222222;
  }

}
