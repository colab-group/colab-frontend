import {Component, Input, OnInit} from '@angular/core';
import {MusicDeezerApiModel} from '../../../../models/MusicDeezerApiModel';

@Component({
  selector: 'app-deezeritem',
  templateUrl: './deezeritem.component.html',
  styleUrls: ['./deezeritem.component.scss']
})
export class DeezeritemComponent implements OnInit {


  width: number;
  height: number;
  @Input() deezrItem: MusicDeezerApiModel;
  constructor() {
    this.setWidth(300);
  }

  ngOnInit() {
  }

  setWidth(width: number) {
    this.width = width;
    this.height = width * 0.56222222;
  }

}
