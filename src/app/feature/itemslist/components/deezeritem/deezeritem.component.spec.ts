import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeezeritemComponent } from './deezeritem.component';

describe('DeezeritemComponent', () => {
  let component: DeezeritemComponent;
  let fixture: ComponentFixture<DeezeritemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeezeritemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeezeritemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
