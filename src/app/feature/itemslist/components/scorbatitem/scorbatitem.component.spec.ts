import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScorbatitemComponent } from './scorbatitem.component';

describe('ScorbatitemComponent', () => {
  let component: ScorbatitemComponent;
  let fixture: ComponentFixture<ScorbatitemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScorbatitemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScorbatitemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
