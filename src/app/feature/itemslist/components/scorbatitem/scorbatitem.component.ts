import { Component, OnInit, Input } from '@angular/core';
import { ScoreBatApiModel } from 'src/app/models/ScoreBatApiModel';

@Component({
  selector: 'app-scorbatitem',
  templateUrl: './scorbatitem.component.html',
  styleUrls: ['./scorbatitem.component.scss']
})
export class ScorbatitemComponent implements OnInit {

  @Input() scorebatitem: ScoreBatApiModel;

  constructor() {
  }

  ngOnInit() {
  }

}
