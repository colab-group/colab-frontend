export enum DisplayModel {
  MOSAIC = 'MOSAIC',
  LIST = 'LIST'
}
