# Colab - Frontend

### Frontend Development

This is where the frontend part of the **Colab Project** is developed.

We're gonna buildup this application with the ***Angular*** framework.

This are the current versions of the module that we are using:

| Mod       | Version           |
| ------------- |:-------------:| 
| Angular CLI      | 11.8.0|
| Angular     | 8.2.14      | 
| Node | 11.8.0      | 


Actually, this is our structure folder application:

    colab-fronted/
     ├──e2e/                                 * -
     |   ├──src                              * source folder of e2e
     |   |  ├──app.e2e-spec.ts               * a simple end-to-end test for /
     |   |  ├──app.po.ts                     * -
     |   ├──protractor.conf.js               * -
     |   └──tsconfig.json                    * -
     ├──node_modules/                        * library root
     |   ├──      WE WILL NOT MENTION ALL THE MODULES IN THIS GRAPH BECAUSE OF THE WIDTH OF THIS LIST
     ├──src/                                 * our source files that will be compiled to javascript
     │   ├──app/                             * WebApp: folder
     |   │  ├──videos                        * Videos component folder
     |   │  |   ├──videos.component.html     * -
     |   │  |   ├──videos.component.scss     * -
     |   │  |   ├──videos.component.spec.ts  * -
     |   │  |   ├──videos.component.ts       * -
     |   │  ├──app.component.scss            * a simple test of components in app.component.ts
     |   │  ├──app.component.spec.ts         * -
     |   │  ├──app.component.ts              * a simple version of our App component components
     |   │  ├──app.module.ts                 * -
     |   │  ├──app-routing.module.ts         * -
     |   ├──assets/                          * -
     |   │  ├──.gitkeep                      * -
     |   ├──environments/                    * -
     |   │  ├──environment.prod.ts           * -
     |   │  ├──environment.ts                * -
     |   ├──favicon.ico                      * -
     |   ├──index.html                       * Index.html: where we generate our index page
     |   ├──main.ts                          * -
     |   ├──polyfills.ts                     * our polyfills file
     |   ├──style.scss                       * -
     |   └──test.ts                          * -
     │
     ├──.editorconfig                  * -
     ├──.gitignore                     * git ignore file
     ├──angular.json                   * -
     ├──browserslist                   * our entry file for our browser environment
     ├──karma.conf.js                  * karma config for our unit tests
     ├──package.json                   * what npm uses to manage its dependencies
     ├──package-lock.json              * -
     ├──README.md                      * Readme of the initial app
     ├──tsconfig.json                  * typescript config used outside webpack 
     ├──tsconfig.app.json              * -
     ├──tsconfig.spec.json             * -
     └──tslint.json                    * typescript lint config


## Run the application

To run the application you need to clone the repo, then run these commands in this order:
```
cd colab-frontend
npm install
ng serve
```
